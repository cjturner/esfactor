# esfactor
Python library for finding factorisations of entanglement spectra or energy spectra.

## References

1. *Optimal free descriptions of many-body theories* - C. J. Turner, K. Meichanetzidis, Z. Papić, J. K. Pachos - Nature Communications 8, 14926 (2017) [[doi:10.1038/ncomms14926]](http://dx.doi.org/10.1038/ncomms14926) [[arXiv:1607.02679]](http://arxiv.org/abs/1607.02679)
2. *Free-fermion descriptions of parafermion chains and string-net models* - K. Meichanetzidis, C. J. Turner, A. Farjami, Z. Papić, J. K. Pachos - [[arXiv:1705.09983]](http://arxiv.org/abs/1705.09983)
