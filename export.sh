# Export environment variables for this repository.
# Usage: source ~/Programming/disorder/export.sh
SOURCE=$(dirname "${BASH_SOURCE[0]}")
export PYTHONPATH="$SOURCE/packages:$SOURCE/submodules/packages:$PYTHONPATH"
