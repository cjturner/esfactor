from __future__ import absolute_import
from functools import partial
from esfactor.cost import TraceD

def _pair_projected(pred, a, b):
  # Combine the two spectra labelling which each level came from
  ap = np.zeros((a.shape[0], a.shape[1]+1))
  bp = np.ones((b.shape[0], b.shape[1]+1))
  ap[:,:-1] = a; bp[:,:-1] = b
  c = np.vstack([ap, bp])

  # Sort the combined spectrum by symmetry charges
  c = c[np.lexsort(c[:,1:-1].T)]

  # Split the spectrum into sectors by symmetry
  splits_c = np.split(c, np.where(np.diff(c[:,1:-1], axis=0))[0] + 1)
  splits_c = [split for split in splits_c if split != np.array([])]

  # Iterate over the sectors
  return ((s[0,1:], np.sort(s[s[:,-1] == 0,0]), np.sort(s[s[:,-1] == 1,0]))
          for s in splits_c if (pred[0,1:]))

def projected(cost, pred, a, b):
  # Rearrange all the data into sectors blocked by symmetry charge
  sectors = _pair_projected(pred,
                            np.vstack([xi_e, xi_q.T]).T,
                            np.vstack([f_e, f_q.T]).T)

  # Calculate the total trace distance across all these sectors
  return sum([cost(np.sort(sector[1]), np.sort(sector[2]))
              for sector in sectors])

TraceD_projected = partial(projected, TraceD)
