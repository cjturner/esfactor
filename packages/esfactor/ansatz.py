from __future__ import print_function
import numpy as np
import time
import sys
import logging
import warnings
from functools import reduce

# Initialise logging system
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
logger.addHandler(logging.StreamHandler(sys.stderr))

def _iterable(x):
  try:
    iter(x)
    return x
  except TypeError:
    return [x]

class ProbabilityAnsatz(object):
  def expand(self, z, m, cutoff=None, chi=None):
    modes = self._pm_to_ps(m, cutoff=cutoff)
    return self.__gen_mpes(z, modes, cutoff=cutoff, chi=chi)

  def expand_with_error(self, z, m, cutoff=None, chi=None):
    modes = self._pm_to_ps(m, cutoff=cutoff)
    return self.__gen_mpes_with_error(z, modes, cutoff=cutoff, chi=chi)

  def expand_unsorted(self, z, m, cutoff=None):
    modes = self._pm_to_ps(m, cutoff=cutoff)
    return self.__unsorted_mpes(z, modes, cutoff=cutoff)

  def expand_charged(self, pz, pm, qz, qm, cutoff=None):
    ps = self._pm_to_ps(m, cutoff=cutoff)
    qs = [self._qm_to_qs(qmj, cutoff=cutoff) for qmj in qm.T]
    return self.__gen_charged_mpes(pz, ps, qz, qs, cutoff=cutoff)

  def normalise(self, m, cutoff=None):
    return 1. / np.prod([np.sum(np.asarray(mode))
                         for mode in self._pm_to_ps(m, cutoff=cutoff)])

  def cutoff_mpes(self, ps, qs, cutoff=None):
    # Sort the spectrum into decending probability
    ps = np.asarray(ps); qs = np.asarray(qs)
    idx = np.argsort(ps)
    ps = ps[idx[::-1]]; qs = qs[idx[::-1],:]

    # Cutoff the spectrum
    if cutoff is not None:
      cutoff = np.exp(-cutoff)
      idx = ps > cutoff
      ps = ps[idx]; qs = qs[idx]

    # Return the spectrum normalised
    return (1. / np.sum(ps)) * ps, qs

  def guess_factorisation(self, p_k, cutoff=None):
    # Delegate to the entanglement energy implementation
    with warnings.catch_warnings():
      warnings.simplefilter("ignore")
      xi_k = - np.log(p_k)
    z, m = ProductAnsatz().guess_factorisation(xi_k, cutoff)

    # Perform a change of variables
    b = 0.5 * (1. - np.exp(-m)) / (1. + np.exp(-m))
    return 1.0, b

  def guess_charged_factorisation(self, p_k, q_k, cutoff=None):
    # Delegate to the entanglement energy implementation
    with warnings.catch_warnings():
      warnings.simplefilter("ignore")
      xi_k = - np.log(p_k)
    z, m, qz, q = ProductAnsatz().guess_charged_factorisation(xi_k, q_k, cutoff)

    # Perform a change of variables
    b = 0.5 * (1. - np.exp(-m)) / (1. + np.exp(-m))
    return 1.0, b, qz, q

  @staticmethod
  def __unsorted_mpes(z, modes, cutoff=None):
    if cutoff is None:
      def fuse(a, b):
        return np.multiply.outer(a,b).flatten()
    else:
      cutoff = np.exp(-cutoff)
      def fuse(a, b):
        c = np.multiply.outer(a,b).flatten()
        return c[c > cutoff]
    ps = reduce(fuse, modes, [z])
    return ps

  @staticmethod
  def __gen_mpes_with_error(z, modes, cutoff=None, chi=None):
    x = z
    error = 0
    cutoff = np.exp(-cutoff) if cutoff is not None else None
    for m in modes:
      # Take all combinations
      x = np.multiply.outer(x, m).flatten()

      # Cut off small probabilities
      if cutoff is not None:
        error += np.sum(x[x < cutoff])
        x = x[x >= cutoff]

      # Cut off trailing elements
      if chi is not None and x.size > chi:
        x[::-1].sort()
        error += np.sum(x[chi:])
        x = x[:chi]

    return x, error

  @staticmethod
  def __gen_mpes(z, modes, cutoff=None, chi=None):
    x, _ = ProbabilityAnsatz.__gen_mpes_with_error(z, modes, cutoff=cutoff, chi=chi)
    return x

  @staticmethod
  def __gen_charged_mpes(z, modes, cutoff=None, chi=None):
    if chi is not None:
      raise NotImplementedError

    if cutoff is None:
      # Generate all the combinations
      def fuse(a, b):
        cp = np.multiply.outer(a[0],b[0]).flatten()
        cq = [np.add.outer(aqr,bqr).flatten() for aqr, bqr in zip(a[1:],bq[1:])]
        return [cp] + cq
    else:
      # Generate all the combinations below the cutoff
      cutoff = np.exp(-cutoff)
      def fuse(a, b):
        cp = np.multiply.outer(a[0],b[0]).flatten()
        cq = [np.add.outer(aqr,bqr).flatten() for aqr, bqr in zip(a[1:],b[1:])]
        idx = cp > cutoff
        c = [cp] + cq
        return [cr[idx] for cr in c]

    R = reduce(fuse, zip(modes, *charges), [z] + list(qz))

    # Sort the spectrum into decending probability
    idx = np.argsort(R[0])
    return R[0][idx[::-1]], np.vstack([Rj[idx[::-1]] for Rj in R[1:]]).T

class FermionProbabilityAnsatz(ProbabilityAnsatz):
  def __init__(self):
    pass

  def _pm_to_ps(self, pm, cutoff=None):
    return [(0.5 - pmk, 0.5 + pmk) for pmk in np.minimum(np.abs(pm), 0.5)]

  def _qm_to_qs(self, qm, cutoff=None):
    return [(0, qmk) for qmk in qm]

class ProductAnsatz(object):
  def expand(self, z, m, cutoff=None):
    modes = self._m_to_modes(m, cutoff=cutoff)
    return self.__gen_mpes(z, modes, cutoff=cutoff)

  def expand_with_error(self, z, m, cutoff=None, chi=None):
    modes = self._m_to_modes(m, cutoff=cutoff)
    return self.__gen_mpes_with_error(z, modes, cutoff=cutoff, chi=chi)

  def expand_unsorted(self, z, m , cutoff=None):
    modes = self._m_to_modes(m, cutoff=cutoff)
    return self.__unsorted_mpes(z, modes, cutoff=cutoff)

  def expand_charged(self, z, m, qz, qm, cutoff=None, chi=None):
    es_modes = self._m_to_modes(m, cutoff=cutoff)
    qs_modes = [self._m_to_modes(qj, cutoff=cutoff) for qj in np.asarray(qm).T]
    return self.__gen_charged_mpes(z, es_modes, qz, qs_modes,
                                   cutoff=cutoff, chi=chi)

  def normalise(self, m, cutoff=None):
    return np.log(np.prod([np.sum(np.exp(-np.asarray(mode)))
                           for mode in self._m_to_modes(m, cutoff=cutoff)]))

  def cutoff_mpes(self, xi_k, q_k, cutoff=None):
    # Sort, cutoff and normalise the given many-body spectrum
    idx = np.argsort(xi_k)
    xi_k = np.asarray(xi_k)[idx]; q_k[idx]
    if cutoff is not None:
      idx = xi_k < cutoff
      xi_k = xi_k[idx]; q_k = q_k[idx]
    xi_k += np.log(np.sum(np.exp(-xi_k)))
    return xi_k, q_k

  def guess_factorisation(self, xi_k, cutoff=None):
    # Produce entanglement spectrum cutoff and normalised
    idx = np.argsort(xi_k)
    xi_k = np.array(xi_k)[idx]
    if cutoff is not None:
      xi_k = xi_k[xi_k < cutoff]
    xi_k += np.log(np.sum(np.exp(-xi_k)))

    # Pick the lowest level as z
    z_idx = np.argmin(xi_k)
    z = xi_k[z_idx]
    xi_k = np.delete(xi_k, z_idx)

    # Iteratively pick entanglement energies for the modes
    m_k = []
    f_cache = np.array([z])
    t0 = time.time()
    while xi_k.size != 0:
      # Take the lowest entanglement energy from the set
      m_idx = np.argmin(xi_k)
      m_r = xi_k[m_idx]
      if not np.isfinite(m_r):
        xi_k = np.delete(xi_k, m_idx)
        continue
      m_r -= z

      # Generate all the new combinations with this new single particle level
      f_new = f_cache + m_r
      if cutoff is not None:
        f_new = f_new[f_new < cutoff]
      m_k.append(m_r)
      f_cache = np.hstack([f_cache, f_new])

      # Remove nearest level
      for r in range(f_new.size):
        distance = np.abs(xi_k - f_new[r])
        idx = np.argmin(distance)

        # Ignore the level if there isn't anything sufficiently nearby
        if distance[idx] < 1:
          xi_k = np.delete(xi_k, idx)
          if xi_k.size == 0: break

      t = time.time()
      logger.info('len(xi) = %s, len(m) = %s, m = {... %s} in %s seconds.',
                  xi_k.size, len(m_k), m_r, t - t0)
      t0 = t

    logger.info('Factorisation has length %s', len(m_k))
    return z, np.array(m_k)

  def guess_charged_factorisation(self, xi_k, q_k=None, cutoff=None):
    if q_k is None:
      q_k = np.zeros((len(xi_k),0))
    xi_k = np.asarray(xi_k)
    chi = xi_k.size

    # Produce entanglement spectrum cutoff and normalised
    idx = np.argsort(xi_k)
    xi_k = np.array(xi_k)[idx]
    q_k = np.array(q_k)[idx]
    if cutoff is not None:
      idx = xi_k < cutoff
      xi_k = xi_k[idx]; q_k = q_k[idx]
    xi_k += np.log(np.sum(np.exp(-xi_k)))

    # Pick the lowest level as z
    z_idx = np.argmin(xi_k)
    z = xi_k[z_idx]
    qz = q_k[z_idx]
    xi_k = np.delete(xi_k, z_idx)
    q_k = np.delete(q_k, z_idx, axis=0)
    logger.info('Base point z = %s, qz = %s', z, qz)

    # Iteratively pick entanglement energies for the modes
    m = []; qm = []
    fe_cache = np.array([z])
    fq_cache = np.array([qz])
    t0 = time.time()
    acc = np.exp(-z)
    while xi_k.size != 0 and acc < 1.1:
      for s in range(xi_k.size):
        if xi_k[s] < 1:
          logger.debug('Level %s: xi = %s, q = %s', s, xi_k[s], q_k[s])

      # Take the lowest entanglement energy from the set
      m_idx = np.argmin(xi_k)
      m_r = xi_k[m_idx]; q_r = q_k[m_idx]

      logger.info('Picked xi = %s, q = %s', m_r, q_r)
      if not np.isfinite(m_r):
        xi_k = np.delete(xi_k, m_idx)
        q_k = np.delete(q_k, m_idx, axis=0)
        continue
      m_r = m_r - z; q_r = q_r - qz
      acc *= (1 + np.exp(-m_r))
      logger.info('Guessed mode em = %s, qm = %s, acc = %s', m_r, q_r, acc)

      # Generate all the new combinations with this new single particle level
      fe_new = fe_cache + m_r
      fq_new = fq_cache + q_r
      if cutoff is not None:
        idx = fe_new < cutoff
        fe_new = fe_new[idx]; fq_new = fq_new[idx]
      m.append(m_r); qm.append(q_r)
      fe_cache = np.hstack([fe_cache, fe_new])
      fq_cache = np.vstack([fq_cache, fq_new])

      # Remove nearest level
      for r in range(fe_new.size):
        distance = np.abs(xi_k - fe_new[r])
        mask = np.all(q_k == fq_new[r], axis=1)
        mask = mask.nonzero()[0]

        if mask.size != 0:
          idx = mask[np.argmin(distance[mask])]
        else:
          continue

        # Ignore the level if there isn't anything sufficiently nearby
        if distance[idx] < 1:
          logger.debug('Matched fe = %s, fq = %s with xi = %s, q = %s at %s',
                      fe_new[r], fq_new[r], xi_k[idx], q_k[idx], idx)
          logger.debug('Distance is %s', distance[idx])
          xi_k = np.delete(xi_k, idx)
          q_k = np.delete(q_k, idx, axis=0)
          if xi_k.size == 0: break
        else:
          logger.debug("Failed to match fe = %s, fq = %s", fe_new[r], fq_new[r])

      t = time.time()
      logger.info('len(fe_cache) = %s', fe_cache.size)
      logger.info('len(xi) = %s, len(m) = %s, m = {... %s} in %s seconds.',
                  xi_k.size, len(m), m_r, t - t0)
      t0 = t

      # Remove levels which go past the end of the input spectrum
      if fe_cache.size > chi:
        idx = np.argsort(fe_cache)
        fe_cache = fe_cache[idx]; fq_cache = fq_cache[idx]
        fe_cache = fe_cache[:chi]; fq_cache = fq_cache[:chi]

    logger.info('Factorisation has length %s', len(m))
    return z, np.atleast_1d(m), np.atleast_1d(qz), np.atleast_2d(qm)

  @staticmethod
  def _gen_charged_mpes(z, modes, az, charges, cutoff=None):
    return ProductAnsatz.__gen_charged_mpes(z, modes, az, charges, cutoff=cutoff)

  @staticmethod
  def __unsorted_mpes(z, modes, cutoff=None):
    if cutoff is None:
      def fuse(a, b):
        return np.add.outer(a,b).flatten()
    else:
      def fuse(a, b):
        c = np.add.outer(a,b).flatten()
        return c[c < cutoff]
    return np.atleast_1d(reduce(fuse, modes, [z]))

  @staticmethod
  def __gen_mpes(z, modes, cutoff=None):
    return np.sort(ProductAnsatz.__unsorted_mpes(z, modes, cutoff=cutoff))

  @staticmethod
  def __gen_mpes_with_error(z, modes, cutoff=None, chi=None):
    x = z
    error = 0
    for m in modes:
      # Take all combinations
      x = np.add.outer(x, m).flatten()
      error = error * np.sum(np.exp(-np.asarray(m)))

      # Cut off small probabilities
      if cutoff is not None:
        error += np.sum(np.exp(-x[x > cutoff]))
        x = x[x <= cutoff]

      # Cut off trailing elements
      if chi is not None and x.size > chi:
        x.sort()
        error += np.sum(np.exp(-x[chi:]))
        x = x[:chi]

    return np.sort(np.atleast_1d(x)), error

  @staticmethod
  def __gen_charged_mpes(z, modes, qz, charges, cutoff=None):
    if cutoff is None:
      # Generate all the combinations
      def fuse(a, b):
        return [np.add.outer(ar,br).flatten() for ar, br in zip(a,b)]
    else:
      # Generate all the combinations below the cutoff
      def fuse(a, b):
        c = [np.add.outer(ar,br).flatten() for ar, br in zip(a,b)]
        idx = c[0] < cutoff
        return [cr[idx] for cr in c]
    R = reduce(fuse,
               zip(modes,*charges),
               [np.array([x]) for x in [z] + list(qz)])

    # Sort the spectrum by xi
    idx = np.argsort(R[0])
    return R[0][idx], np.vstack([Rj[idx] for Rj in R[1:]]).T

class FermionAnsatz(ProductAnsatz):
  def __init__(self):
    pass
  
  def _m_to_modes(self, m, cutoff=None):
    return [(0, abs(mk)) for mk in m]

class HardCoreBosonAnsatz(ProductAnsatz):
  def __init__(self, k):
    """
    Parameters
    ----------
    k : The number of retained states, i.e. Occupation numbers are 0,1,...,k-1.
    """
    self.__k = k
      
  def _m_to_modes(self, m, cutoff=None):
    return [np.arange(0, self.__k) * abs(mk) for mk in m]
    
class BosonAnsatz(ProductAnsatz):
  """
  You should use the HardCodeBosonAnsatz instead, and either bound the
  systematic error introduced by the occupation cutoff using the geometric
  series or do some other kind of convergence analysis.
  """
  def __init__(self):
    pass
  
  def _m_to_modes(self, m, cutoff=None):
    return [np.arange(mk, cutoff, step=abs(mk)) for mk in m]
    
class CyclicAnsatz(ProductAnsatz):
  """
  Expands a spectrum factorised as a tensor product of fundamental
  representations of the Cyclic group Z_d
  """
  def __init__(self, d):
    self.__d = d
  
  def _m_to_modes(self, m, cutoff=None):
    for mk in m:
        if len(_iterable(mk)) != (self.__d - 1):
            raise ValueError("Incorrect number of levels for Z_{}."
                             .format(self.__d))
    return m

  def guess_factorisation(self, xi):
    raise NotImplementedError
    
class DihedralAnsatz(ProductAnsatz):
  """
  Expands a spectrum factorised as a tensor product of fundamental
  representations of the Dihedral group Dih(2d).
  """
  def __init__(self, d):
    self.__d = d
  
  def _m_to_modes(self, m, cutoff=None):
    for mk in m:
        if len(_iterable(mk)) != (self.__d // 2 ):
            raise ValueError("Incorrect number of levels for Dih({})."
                             .format(self.__d))
    return [np.repeat(mk, 2)[:self.__d] for mk in m]

  def guess_factorisation(self, xi):
    raise NotImplementedError

class PolynomialFermionAnsatz(ProductAnsatz):
  """
  Builds a fermion ansatz where the single particle entanglement
  energies are taken from a polynomial.
  """
  def __init__(self, order=4, cutoff=None, max_k=40):
    self.__order = order
    self.__cutoff = cutoff
    self.__max_k = max_k

  def guess_factorisation(self, xi_k, cutoff=None):
    # Produce entanglement spectrum cutoff and normalised
    xi_k = np.sort(np.atleast_1d(xi_k))
    if cutoff is not None:
      xi_k = xi_k[xi_k < cutoff]
    xi_k += np.log(np.sum(np.exp(-xi_k)))

    # Completely blind guess for the ms 
    m = np.ones(self.__order)

    # Ensure normalisation
    z = self.normalise(m, cutoff=cutoff)
    return z, m

  def _m_to_modes(self, m, cutoff=None):
    if cutoff is None:
      if self.__cutoff is None:
        raise ValueError("PolynomialFermionAnsatz requires a xi cutoff.")
      cutoff = self.__cutoff
    modes = []
    for k in range(self.__max_k):
      mode = sum((m[r] * k ** r) for r in range(self.__order))
      if mode > cutoff:
        break
      modes.append((0,mode))
    return modes

if __name__ == '__main__':
  ansatz = PolynomialFermionAnsatz()
  modes = ansatz._m_to_modes([
              -1.4541127274664254, 
              0.27189409203269677, 
              0.32752318637027683, 
              0.6555907704587758
            ], cutoff=100)
  print(modes)

  print(FermionAnsatz().expand_charged(0, [2,3], [0,0], [[1,-1],[1,1]],
                                       cutoff=None))
