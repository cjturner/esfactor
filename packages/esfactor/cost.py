import numpy as np

def TraceD(xi_k, f_k, error=0):
  # Pad the spectra to a consistent size
  size = max(len(xi_k),len(f_k))
  s_k = np.zeros((size,))
  s_k[:len(f_k)] = np.exp(-np.asarray(f_k))
  p_k = np.zeros((size,))
  p_k[:len(xi_k)] = np.exp(-np.asarray(xi_k))

  # Evaluate the expression for the diagonal trace distance
  T_k = np.abs(p_k - s_k)
  return 1./2. * (np.sum(T_k) + error)

class BetaTraceD(object):
  def __init__(self, beta=1.):
    self.beta = beta

  def __call__(self, xi_k, f_k, error=0):
    if error != 0:
      raise NotImplementedError

     # Pad the spectra to a consistent size
    size = max(len(xi_k),len(f_k))
    s_k = np.zeros((size,))
    s_k[:len(f_k)] = np.exp(-self.beta*np.asarray(f_k))
    p_k = np.zeros((size,))
    p_k[:len(xi_k)] = np.exp(-self.beta*np.asarray(xi_k))

    # Evaluate the expression for the diagonal trace distance
    T_k = np.abs(p_k - s_k)
    return 1./2. * np.sum(T_k)

def TraceD_p(ps, fs, error=0):
  # Pad the spectra to a consistent size
  size = max(len(ps),len(fs))
  ps = np.pad(ps, (0,size-len(ps)), mode='constant', constant_values=[0.])
  fs = np.pad(fs, (0,size-len(fs)), mode='constant', constant_values=[0.])

  # Evaluate the expression for the diagonal trace distance
  return 0.5 * (np.sum(np.abs(ps - fs)) + error)

def RelE(xi_k, f_k, error=0):
  if error != 0:
    raise NotImplementedError

  # Pad the spectra to a consistent size
  size = max(len(xi_k),len(f_k))
  s_k = np.ones((size,))
  s_k *= 100.
  s_k[:len(f_k)] = f_k
  p_k = np.ones((size,))
  p_k *= 100.
  p_k[:len(xi_k)] = xi_k

  # Evaluate the expression for the diagonal quantum relative entropy
  S = np.exp(-p_k).dot(s_k - p_k)
  return np.min(S, 0)
