import logging
import sys
import time
import numpy as np
from esfactor.ansatz import FermionAnsatz
from scipy.optimize import minimize, basinhopping
import warnings

# Initialise logging system
logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
logger.addHandler(logging.StreamHandler(sys.stderr))

def minimise_normed(cost_func,
                    xi_k,
                    q_k=None,
                    x0=None,
                    method='Nelder-Mead',
                    ftol=1e-11,
                    xtol=None,
                    hopping=False,
                    basins=100,
                    stepsz=1.0,
                    temperature=5.0,
                    ansatz=FermionAnsatz(),
                    cutoff=100):
  warnings.warn(message, DeprecationWarning, stacklevel=2)
  return minimise(cost_func=cost_func, xi_k=xi_k, q_k=q_k, x0=x0,
                  method=method, ftol=ftol, xtol=xtol, hopping=hopping,
                  basins=basins, stepsz=stepsz, temperature=temperature,
                  ansatz=ansatz, cutoff=cutoff, normed=True)

def minimise(cost_func,
             xi_k,
             q_k=None,
             x0=None,
             method='Nelder-Mead',
             ftol=1e-11,
             xtol=None,
             hopping=False,
             basins=100,
             stepsz=1.0,
             temperature=5.0,
             ansatz=FermionAnsatz(),
             cutoff=100,
             normed=True):
  t0 = time.time()

  # Prepare the initial guess vector
  if x0 is None:
    z, m, qz, qm = ansatz.guess_charged_factorisation(xi_k, q_k, cutoff=cutoff)
  else:
    if normed:
      if q_k is None:
        m = x0
      else:
        m, qz, qm = x0
      z = ansatz.normalise(m, cutoff=cutoff)
    else:
      if q_k is None:
        z, m = x0
      else:
        z, m, qz, qm = x0
  if normed:
    x0 = m
  else:
    x0 = np.zeros(shape=len(m)+1, dtype=np.float64)
    x0[0] = z
    x0[1:] = m

  # Default to trivial charges
  if q_k is None:
    q_k = np.zeros((np.asarray(xi_k).size,0))
    qz = None
    qm = None

  # Produce entanglement spectrum cutoff and normalised
  xi_k, q_k = ansatz.cutoff_mpes(xi_k, q_k, cutoff=cutoff)
  chi = xi_k.size

  # Perform the minimisation
  if normed:
    def helper(x0, xi, q):
      m = x0
      z = ansatz.normalise(m, cutoff=cutoff)
      f_k, error = ansatz.expand_with_error(z, m, cutoff=cutoff, chi=chi)
      f_k = np.asarray(f_k)
      return cost_func(xi, f_k, error=error)
  else:
    def helper(x0, xi, q):
      z, m = x0[0], x0[1:]
      f_k, error = ansatz.expand_with_error(z, m, cutoff=cutoff, chi=chi)
      f_k = np.asarray(f_k)
      return cost_func(xi, f_k, error=error)

  options = {}
  if xtol is not None: options['xtol'] = xtol
  if ftol is not None: options['ftol'] = ftol
  if hopping:
    callback_vars = {'minima_count': 0, 't_minima': time.time()}
    def callback(x, f, accept):
      callback_vars['minima_count'] += 1
      t = time.time()
      logger.info('Local minimum %s in %s seconds.',
                  callback_vars['minima_count'],
                  t - callback_vars['t_minima'])
      callback_vars['t_minima'] = t
      return False
    opt = basinhopping(helper, x0,
                       stepsize=stepsz, T=temperature,
                       niter=basins,
                       minimizer_kwargs={'method': method,
                                         'args':(xi_k,q_k),
                                         'options': options},
                       callback=callback,
                       disp=False)
    opt.success = (opt.message == ['requested number of basinhopping iterations'
                                   ' completed successfully'])
  else:
    opt = minimize(helper, x0=x0, args=(xi_k,q_k), method=method,
                   options=options)

  # Fix down conversion to scalar/0D arrays
  if np.ndim(opt.x) == 0:
    opt.x = np.array(opt.x).reshape(1)

  # Package the results as a dictionary
  return {
    'ansatz': type(ansatz).__name__,
    'cost': opt.fun,
    'z': ansatz.normalise(opt.x, cutoff=cutoff) if normed else opt.x[0],
    'm_k': np.array(opt.x if normed else opt.x[1:]),
    'qz': qz,
    'qm': qm,
    'converged': opt.success,
    'T': time.time() - t0,
    'N': (opt.nit, opt.nfev),
    'method': method,
    'hopping': hopping,
    'normed': normed,
    'ftol': ftol,
    'xtol': xtol
  }
