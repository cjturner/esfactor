import numpy as np
from esfactor.cost import TraceD
from esfactor.ansatz import FermionAnsatz
from esfactor.optimise import minimise as find_df

for h_z in [0.75]:
  print("h_z =", h_z)
  L = 8; h_z = 0.5
  sigma_x = np.array([[0,1],[1,0]])
  sigma_z = np.diag([1,-1])
  inj1 = lambda O, j: np.kron(np.kron(np.eye(2 ** j), O), np.eye(2 ** (L-j-1)))

  H_xx = sum(- inj1(sigma_x, j) @ inj1(sigma_x, (j+1) % L) for j in range(L))
  H_z = h_z * sum(- inj1(sigma_z, j) for j in range(L))

  H = H_xx + H_z

  evals, evecs = np.linalg.eigh(H)
  gs = evecs[:,0]

  M = gs.reshape(2 ** (L//2), 2 ** (L - L//2))
  Sigma = np.linalg.svd(M, compute_uv=False)

  xi = -np.log(Sigma**2)

  print(repr(xi))

  opt_res = find_df(TraceD, xi, ansatz=FermionAnsatz(), normed=True,
                    hopping=True, temperature=1.0, stepsz=0.1, cutoff=30)

  print(opt_res['cost'])
  print(opt_res)

  f, error = FermionAnsatz().expand_with_error(opt_res['z'], opt_res['m_k'], cutoff=30)
  print('expansion', f)

  print('lower cost', TraceD(xi, np.sort(f)))
  print('error', error)

  if opt_res['cost'] > 1.0e-12:
    exit(1) # Bad
  else:
    exit(0) # Good
