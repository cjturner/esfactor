import logging
import argparse
import sys
import numpy as np
from esfactor.cost import TraceD, TraceD_p
from esfactor.ansatz import FermionAnsatz, FermionProbabilityAnsatz
from esfactor.optimise import minimise

# Configure logging to stderr
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stderr))

def __main():
  desc = """Calculate DF from entanglement spectrum."""
  parser = argparse.ArgumentParser(description=desc)
  parser.add_argument('infile', type=str, default=None)
  args = parser.parse_args()

  # Load the input data file
  indata = np.loadtxt(args.infile)

  # Compatability with single spectrum input
  if indata.ndim == 1:
    indata = indata.reshape(1,indata.size)

  for j in range(indata.shape[0]):
    es = indata[j,:]
    result = minimise(TraceD, es, ansatz=FermionAnsatz(), hopping=True,
                      normed=True, cutoff=100)
    result['m_k'] = result['m_k'].tolist()
    result['N'] = list(result['N'])
    print(repr(result))

if __name__ == '__main__':
  __main()
